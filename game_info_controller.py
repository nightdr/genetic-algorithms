import re
import time
import win32gui
import win32ui
import win32con
from PIL import Image


class GameInfoController:
    def __init__(self, info_filepath, window_handle, resize_width, resize_height):
        self.info_filepath = info_filepath
        self.window_handle = window_handle
        self.resize_width = resize_width
        self.resize_height = resize_height
        self.set_file_info()
        self.set_pixel_values()

    # get died, x_pos, and timer info from file and set variables
    def set_file_info(self):
        with open(self.info_filepath) as f:
            lines = f.readlines()

            while not lines:
                time.sleep(0.01)
                lines = f.readlines()

            died_line = lines[0]
            x_line = lines[1]
            time_line = lines[2]

            died_int = int(re.findall("\d+", died_line)[0])
            x_pos = int(re.findall("\d+", x_line)[0])
            timer = int(re.findall("\d+", time_line)[0])

            if died_int == 0:
                died_bool = False
            else:
                died_bool = True

            self.died_bool = died_bool
            self.x_pos = x_pos
            self.timer = timer

    # get screen pixel values and set them as pixel_data
    def set_pixel_values(self):
        # get rectangle of client area
        l, t, r, b = win32gui.GetClientRect(self.window_handle)
        width = r - l
        height = b - t

        # get and create appropriate dcs
        src_dc_handle = win32gui.GetDC(self.window_handle)
        memory_dc = win32ui.CreateDCFromHandle(src_dc_handle)
        save_dc = memory_dc.CreateCompatibleDC()

        # create bitmap
        bitmap = win32ui.CreateBitmap()
        bitmap.CreateCompatibleBitmap(memory_dc, width, height)

        save_dc.SelectObject(bitmap)

        # capture client area of save_dc
        save_dc.BitBlt((0, 0), (width, height), memory_dc, (0, 0), win32con.SRCCOPY)

        # get info from the bitmap and the bitmap's pixel values
        # bitmap.GetBitmapBits(False) gives (g, r, b, a)
        bmpstr = bitmap.GetBitmapBits(True)

        # convert bitmap pixel values into a PIL Image
        im = Image.frombuffer("RGB", (width, height), bmpstr, "raw", "BGRX", 0, 1)

        # convert to greyscale, resize, and then make the pixel values between 0 and 1
        im = im.convert("L")
        im = im.resize((self.resize_width, self.resize_height))
        data = list(im.getdata())
        data = [val/256 for val in data]

        # memory cleanup
        memory_dc.DeleteDC()
        save_dc.DeleteDC()
        win32gui.ReleaseDC(self.window_handle, src_dc_handle)
        win32gui.DeleteObject(bitmap.GetHandle())

        self.pixel_data = data

    # getters
    def get_x_pos(self):
        self.set_file_info()
        return self.x_pos

    def get_died_bool(self):
        self.set_file_info()
        return self.died_bool

    def get_timer_int(self):
        self.set_file_info()
        return self.timer

    def get_pixel_data(self):
        self.set_pixel_values()
        return self.pixel_data