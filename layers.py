import numpy as np


# fully connected model framework
class FullyConnectedLayer:
    # set object vars
    def __init__(self, num_of_ins):
        self.num_of_ins = num_of_ins
        self.create_weights_and_biases()

    # create weights and biases
    def create_weights_and_biases(self):
        self.weights = np.random.normal(loc=0, scale=1, size=(self.num_of_ins, self.num_of_ins))
        self.biases = np.random.normal(loc=0, scale=1, size=self.num_of_ins)

    # layer output
    def get_output(self, inputs):
        return np.add(np.dot(inputs, self.weights), self.biases)

    # getters and setters
    def get_weights(self):
        return self.weights

    def get_biases(self):
        return self.biases

    def set_weights(self, weights):
        self.weights = weights

    def set_biases(self, biases):
        self.biases = biases


# extends FullyConnectedLayer
class LastFullyConnectedLayer(FullyConnectedLayer):
    # set object vars
    def __init__(self, num_of_ins, num_of_outs):
        self.num_of_outs = num_of_outs
        super().__init__(num_of_ins)

    # create weights and biases
    # override with weights and biases being different sizes
    def create_weights_and_biases(self):
        self.weights = np.random.normal(loc=0, scale=1, size=(self.num_of_ins, self.num_of_outs))
        self.biases = np.random.normal(loc=0, scale=1, size=self.num_of_outs)
