import numpy as np
from heapq import nlargest


class FitnessController:
    def __init__(self, model_dict):
        self.model_dict = model_dict

    # get best model by fitness
    def get_fittest_model(self):
        # sort dict by values/fitnesses (order is descending)
        sorted_dict = dict(sorted(self.model_dict.items(), key=lambda x: x[1], reverse=True))
        # return first model (model with highest fitness)
        return list(sorted_dict.keys())[0]

    def get_fittest_models(self, n):
        return nlargest(n, self.model_dict, key=self.model_dict.get)

    # return highest fitness of models
    def get_highest_fitness(self):
        # gets the max fitness of the fitnesses of the models
        return max(list(self.model_dict.values()))

    def get_mean_fitness(self):
        return np.mean(list(self.model_dict.values()))
