import random


class SelectionController:
    def __init__(self, model_dict):
        self.model_dict = model_dict
        self.models = list(self.model_dict.keys())

    # randomly select 2 models from the model array
    def roulette_selection(self):
        if len(self.models) > 1:
            first_index = random.randint(0, len(self.models) - 1)
            second_index = random.randint(0, len(self.models) - 1)
            while first_index == second_index:
                second_index = random.randint(0, len(self.models) - 1)
            return self.models[first_index], self.models[second_index]

    # randomly select 2 individuals with weights based on fitnesses
    def weighted_roulette_selection(self):
        if len(self.models) > 1:
            fitnesses = list(self.model_dict.values())
            sum_of_fitnesses = sum(fitnesses)

            weights = []
            for fitness in fitnesses:
                weights.append(fitness/sum_of_fitnesses)

            indecies = list(range(0, len(self.models)))

            first_index = random.choices(indecies, weights)[0]
            second_index = random.choices(indecies, weights)[0]
            while first_index == second_index:
                second_index = random.choices(indecies, weights)[0]

            return self.models[first_index], self.models[second_index]

    # randomly select 1(IMPORTANT) individual based on tournament selection
    def tournament_select(self, contestants):
        # check if there will be sufficient contestants available to pick
        if len(self.models) >= contestants:
            new_dict = {}
            # random selection
            for i in range(0, contestants):
                key, value = random.choice(list(self.model_dict.items()))
                # if key is already in
                while key in new_dict:
                    key, value = random.choice(list(self.model_dict.items()))
                new_dict[key] = value

            return_model = None

            # pick fittest of selected models
            for model, fitness in new_dict.items():
                if not return_model:
                    return_model = model
                elif fitness > new_dict[return_model]:
                    return_model = model

            return return_model
