from win32api import keybd_event
import time


class KeyController:
    # hex key codes
    f1 = 0x70

    left_arrow = 0x25
    up_arrow = 0x26
    right_arrow = 0x27
    down_arrow = 0x28

    d = 0x44
    f = 0x46


    # button pushing functions
    @staticmethod
    def hold_key(key, hold_time):
        # 0x0001 = extendedKey press
        keybd_event(key, 0, 0x0001, 0)
        time.sleep(hold_time)
        # 0x0002 = keyUp
        keybd_event(key, 0, 0x0002, 0)

    @staticmethod
    def push_key(key):
        # 0x0001 = extendedKey press
        keybd_event(key, 0, 0x0001, 0)

    @staticmethod
    def release_key(key):
        # 0x0002 = keyUp
        keybd_event(key, 0, 0x0002, 0)

    @staticmethod
    # release all arrow keys, d, and f
    def release_all_keys():
        keys = [KeyController.left_arrow, KeyController.up_arrow, KeyController.right_arrow, KeyController.down_arrow,
                KeyController.d, KeyController.f]

        for key in keys:
            KeyController.release_key(key)
