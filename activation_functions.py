import numpy as np


class ActivationFunctions:
    # non-linear activation function used to squish (-inf, inf) to (0, 1)
    # has vanishing gradients, use relu if this is a problem
    @staticmethod
    def sigmoid(x):
        return 1. / (1. + np.exp(-x))
