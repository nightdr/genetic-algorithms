from algorithm_controller import AlgorithmController

if __name__ == "__main__":
    # hyper parameters
    population_size = 50
    resize_width, resize_height = 100, 100
    num_of_outputs = 6
    num_of_layers = 1
    num_of_generations = 100

    run_controller = AlgorithmController(population_size, resize_width, resize_height, num_of_outputs, num_of_layers,
                                         num_of_generations)

    run_controller.run()
