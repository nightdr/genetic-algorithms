from layers import LastFullyConnectedLayer
from layers import FullyConnectedLayer
from activation_functions import ActivationFunctions
sigmoid = ActivationFunctions.sigmoid


class FullyConnectedNN:
    # initialization of variables
    def __init__(self, num_of_ins, num_of_outs, num_of_layers):
        self.num_of_ins = num_of_ins
        self.num_of_outs = num_of_outs
        self.num_of_layers = num_of_layers
        self.layers = self.generate_layers()

    # generate layers
    def generate_layers(self):
        return_array = []
        for i in range(0, self.num_of_layers):
            if i == self.num_of_layers - 1:
                layer = LastFullyConnectedLayer(self.num_of_ins, self.num_of_outs)
            else:
                layer = FullyConnectedLayer(self.num_of_ins)
            return_array.append(layer)
        return return_array

    # output of all layers using sigmoid
    def get_output(self, inputs):
        logits = None
        for i in range(0, len(self.layers)):
            layer = self.layers[i]
            if len(self.layers) == 1:
                logits = layer.get_output(inputs)
            else:
                if i == 0:
                    logits = sigmoid(layer.get_output(inputs))
                elif i == len(self.layers) - 1:
                    logits = layer.get_output(logits)
                else:
                    logits = sigmoid(layer.get_output(logits))
        return logits

    # getters and setters
    def get_weights(self):
        weights = []
        for layer in self.layers:
            weights.append(layer.get_weights())
        return weights

    def get_biases(self):
        biases = []
        for layer in self.layers:
            biases.append(layer.get_biases())
        return biases

    def get_layers(self):
        return self.layers

    # returns num of weights assuming normal model
    def get_num_of_weights(self):
        return self.num_of_ins**2 * (len(self.layers) - 1) + self.num_of_ins * self.num_of_outs

    # return num of biases assuming normal model
    def get_num_of_biases(self):
        return self.num_of_ins * (len(self.layers) - 1) + self.num_of_outs

    def set_weights(self, weights):
        for i in range(0, len(self.layers)):
            self.layers[i].set_weights(weights[i])

    def set_biases(self, biases):
        for i in range(0, len(self.layers)):
            self.layers[i].set_biases(biases[i])
