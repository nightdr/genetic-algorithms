import numpy as np
import random


class CrossoverController:
    def __init__(self, crossover_prob):
        self.crossover_prob = crossover_prob

    # select one random point of the arrays and swap every value after that
    @staticmethod
    def one_point_crossover(array1, array2):
        original_shape = np.array(array1).shape

        array1 = np.reshape(array1, (-1))
        array2 = np.reshape(array2, (-1))

        point = random.randint(0, len(array1) - 1)

        for i in range(0, len(array1)):
            if i >= point:
                first = array1[i]
                second = array2[i]
                array1[i] = second
                array2[i] = first

        array1 = np.reshape(array1, original_shape)
        array2 = np.reshape(array2, original_shape)

        return array1, array2

    # go through each index of the arrays and use prob to determine if they will be swapped
    @staticmethod
    def uniform_crossover(array1, array2, crossover_prob):
        original_shape = np.array(array1).shape

        array1 = np.reshape(array1, (-1))
        array2 = np.reshape(array2, (-1))

        weights = [crossover_prob, (1-crossover_prob)]
        for i in range(0, len(array1)):
            coin_toss = random.choices([0, 1], weights)[0]
            # 0 = crossover, 1 = no crossover
            if coin_toss == 0:
                first = array1[i]
                second = array2[i]
                array1[i] = second
                array2[i] = first

        array1 = np.reshape(array1, original_shape)
        array2 = np.reshape(array2, original_shape)

        return array1, array2

    # crossover the weights and biases of object models using one_point_crossover
    def crossover_models(self):
        weights = [self.crossover_prob, (1-self.crossover_prob)]
        coin_toss = random.choices([0, 1], weights)[0]
        # 0 = crossover, 1 = no crossover

        # crossover weights and biases of each layer
        if coin_toss == 0:
            layers1 = self.model1.get_layers()
            layers2 = self.model2.get_layers()
            for i in range(0, len(layers1)):
                layer1 = layers1[i]
                layer2 = layers2[i]
                new_weights1, new_weights2 = CrossoverController.one_point_crossover(layer1.get_weights(), layer2.get_weights())
                new_biases1, new_biases2 = CrossoverController.one_point_crossover(layer1.get_biases(), layer2.get_biases())

                layer1.set_weights(new_weights1)
                layer2.set_weights(new_weights2)

                layer1.set_biases(new_biases1)
                layer2.set_biases(new_biases2)

    # for setting new models without instantiating a new object
    def set_models(self, model1, model2):
        self.model1 = model1
        self.model2 = model2
