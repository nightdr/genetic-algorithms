from neural_network import FullyConnectedNN
from stalling_thread import StallingThread
from game_info_controller import GameInfoController
from win32gui import FindWindow
from key_controller import KeyController
from fitness_controller import FitnessController
from selection_controller import SelectionController
from crossover_controller import CrossoverController
from mutation_controller import MutationController
import copy
import time


class AlgorithmController:
    def __init__(self, population_size, resize_width, resize_height, num_of_outputs, num_of_layers, num_of_generations):
        self.population_size = population_size
        self.num_of_inputs = resize_width * resize_height
        self.num_of_outputs = num_of_outputs
        self.num_of_layers = num_of_layers
        self.num_of_generations = num_of_generations
        self.fps = 60

        self.elite_pop_size = int(self.population_size * 1/4)
        if self.elite_pop_size % 2 != 0:
            self.elite_pop_size += 1

        self.save_step = 5

        self.crossover_prob = 0.50
        self.tournament_contestants = int(self.population_size * 1/3)

        print("Generating {} models...".format(population_size))
        self.generate_models()
        print("Successfully generated models")

        self.models_to_evaluate = self.models
        self.model_dict = dict.fromkeys(self.models)

        game_info_path = r"C:\Users\David\Desktop\Computer Science\Lua Files\test.txt"
        # gets FCEUX Super Mario Bros. window, change title for finding other applications or ROMs
        window_handle = FindWindow(None, "FCEUX 2.2.2: Super Mario Bros. (Japan, USA)")
        self.game_info_controller = GameInfoController(game_info_path, window_handle, resize_width, resize_height)

        self.stalling_interval = 1
        self.stall_checker = StallingThread(self.stalling_interval, self.game_info_controller)

        self.fitness_controller = FitnessController(self.model_dict)
        self.selection_controller = SelectionController(self.model_dict)
        self.crossover_controller = CrossoverController(self.crossover_prob)

        self.weight_mutation_rate = 100 / self.models[0].get_num_of_weights()
        self.bias_mutation_rate = 100 / self.models[0].get_num_of_biases()
        self.mutation_controller = MutationController(self.weight_mutation_rate, self.bias_mutation_rate)

    # generate a specified number of models and set them in object
    def generate_models(self):
        models = []
        for i in range(0, self.population_size):
            model = FullyConnectedNN(self.num_of_inputs, self.num_of_outputs, self.num_of_layers)
            models.append(model)
        self.models = models
        self.model = self.models[0]

    # turns outputs into key presses
    @staticmethod
    def decode_outputs(outputs):
        for i in range(0, len(outputs)):
            key_array = [KeyController.left_arrow, KeyController.up_arrow, KeyController.right_arrow, KeyController.down_arrow, KeyController.d, KeyController.f]
            # set key
            key = key_array[i]

            # push or release key
            value = outputs[i]
            if value < 0.5:
                KeyController.release_key(key)
            else:
                KeyController.push_key(key)

    # turn outputs into key presses and then set fitness of model
    def evaluate_model(self):
        # you have to create a new thread after stopping the previous thread
        self.stall_checker = StallingThread(self.stalling_interval, self.game_info_controller)
        self.stall_checker.start()
        KeyController.release_all_keys()
        KeyController.hold_key(KeyController.f1, 0.05)

        # run model
        while not self.game_info_controller.get_died_bool() and not self.stall_checker.get_stalling():
            AlgorithmController.decode_outputs(self.model.get_output(self.game_info_controller.get_pixel_data()))
            time.sleep(1/self.fps)

        # set fitness after model dies or stalls
        self.model_dict[self.model] = self.game_info_controller.get_x_pos()
        self.stall_checker.stop()
        KeyController.release_all_keys()

    # just run the model, don't evaluate fitness
    def run_model(self):
        KeyController.release_all_keys()
        KeyController.hold_key(KeyController.f1, 0.05)

        # run model
        while not self.game_info_controller.get_died_bool():
            AlgorithmController.decode_outputs(self.model.get_output(self.game_info_controller.get_pixel_data()))
            time.sleep(1/self.fps)

        KeyController.release_all_keys()

    # here's where the "learning" happens
    def generate_new_population(self):
        new_models = []

        while len(new_models) < self.population_size:
            # implement elitism, then create the rest of the new generation
            if len(new_models) == 0:
                new_models.extend(self.fitness_controller.get_fittest_models(self.elite_pop_size))

            else:
                # select parents
                parent1 = self.selection_controller.tournament_select(self.tournament_contestants)
                parent2 = self.selection_controller.tournament_select(self.tournament_contestants)

                # assign child objects
                # !!!!! deepcopy needed to copy parents without changing them
                child1 = copy.deepcopy(parent1)
                child2 = copy.deepcopy(parent2)

                # crossover
                self.crossover_controller.set_models(child1, child2)
                self.crossover_controller.crossover_models()

                # mutate
                self.mutation_controller.mutate_models([child1, child2])

                new_models.append(child1)
                new_models.append(child2)

        return new_models

    def run(self):
        generation = 0

        # countdown
        for i in range(5, 0, -1):
            print(i)
            time.sleep(1)

        # running GA
        while generation < self.num_of_generations:
            print("Generation: ", generation)

            # evaluate models that need to be evaluated
            for model in self.models_to_evaluate:
                self.model = model
                self.evaluate_model()
                time.sleep(0.1)

            # get best model and print stats
            best_model = self.fitness_controller.get_fittest_model()
            print("Mean fitness: ", self.fitness_controller.get_mean_fitness())
            print("Highest fitness: ", self.fitness_controller.get_highest_fitness())

            print("Fitnesses: ", list(self.model_dict.values()))

            """
            # save models
            if generation % saveStep == 0:
                # create a binary save file to write to based on the run and generation numbers
                directory = "E:\\Genetic Algorithms\\run{0}\\".format(run, generation)

                if not os.path.exists(directory):
                    os.makedirs(directory)

                saveDir = "E:\\Genetic Algorithms\\run{0}\\gen{1}fit{2}.bin".format(run, generation, highestFitness)
                # save best model into file
                with gzip.open(saveDir, "wb") as fi:
                    pickle.dump(best_model, fi, pickle.HIGHEST_PROTOCOL)
                    
                    
            # loading models
            # use obj = pickle.load(file) to reload pickled obj
            
            filePath = "E:\\Genetic Algorithms\\run1\\gen0fit0.bin"
            
            with gzip.open(filePath, "rb") as fi:
                loadedModel = pickle.load(fi)
                runModel(loadedModel, fps)
            """

            # reset models_to_evaluate
            self.models_to_evaluate = []

            # generate new population
            new_models = self.generate_new_population()
            self.models = new_models

            # initialize new_model_dict
            new_model_dict = dict.fromkeys(self.models)

            # check if new_models have been evaluated already, if they have, assign them their given value
            # else, add the model(s) to be evaluated
            for model in self.models:
                if model in self.model_dict:
                    new_model_dict[model] = self.model_dict[model]
                else:
                    self.models_to_evaluate.append(model)

            # set self.model_dict to new_model_dict
            self.model_dict = new_model_dict

            generation += 1
