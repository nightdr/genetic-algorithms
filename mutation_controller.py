from collections import Counter
import numpy as np
import random


class MutationController:
    def __init__(self, weight_prob, bias_prob):
        self.weight_prob = weight_prob
        self.bias_prob = bias_prob

    # swap 2 random points of an array
    @staticmethod
    def swap_mutation(array):
        original_shape = np.array(array).shape

        array = np.reshape(array, (-1))

        if len(array) > 1:
            swap1 = random.randint(0, len(array) - 1)
            swap2 = random.randint(0, len(array) - 1)
            while swap2 == swap1:
                swap2 = random.randint(0, len(array) - 1)

            first = array[swap1]
            second = array[swap2]
            array[swap1] = second
            array[swap2] = first

        array = np.reshape(array, original_shape)

        return array

    @staticmethod
    def reverse_mutation(array):
        # store original shape of array
        original_shape = np.array(array).shape

        # flatten array for work
        array = np.reshape(array, (-1))

        if len(array) > 1:
            point1 = random.randint(0, len(array))
            point2 = random.randint(0, len(array))
            # if points are the same, regenerate point2 until they aren't
            while point2 == point1:
                point2 = random.randint(0, len(array))

            # reverse point 1 and 2 if point1 > point2
            if point1 > point2:
                temp = point1
                point1 = point2
                point2 = temp

            # grab desired slice of array
            sliced_array = array[point1:point2]

            # reverse slice
            sliced_array = sliced_array[::-1]

            # set slice of array in array
            array[point1:point2] = sliced_array

            # reshape array
            array = np.reshape(array, original_shape)

            return array

    @staticmethod
    def random_mutate(array, num_of_times):
        # store original shape of array
        original_shape = np.array(array).shape

        # flatten array for work
        array = np.reshape(array, (-1))

        for i in range(0, num_of_times):
            replace_index = random.randint(0, len(array) - 1)

            new_point = np.random.normal(loc=0, scale=1, size=1)[0]

            array[replace_index] = new_point

        # reshape array
        array = np.reshape(array, original_shape)

        return array

    # mutates each weight and bias genes with random_mutate
    def mutate_model(self):
        # 0 = mutation, 1 = no mutation
        weight_dist = [self.weight_prob, (1-self.weight_prob)]
        bias_dist = [self.bias_prob, (1-self.bias_prob)]

        for layer in self.model.get_layers():
            num_of_weights = len(layer.get_weights())
            num_of_biases = len(layer.get_biases())

            # coinToss = random.choices([0, 1], weights)[0]
            # toss a coin for each weight and bias and count the number of 0s and 1s
            weight_dict = dict(Counter(random.choices([0, 1], weight_dist)[0] for _ in range(0, num_of_weights)))
            bias_dict = dict(Counter(random.choices([0, 1], bias_dist)[0] for _ in range(0, num_of_biases)))

            # get number of weights and biases to be mutated (get number of 0s in each dict)
            if 0 in weight_dict and 0 in bias_dict:
                weight_mutations = weight_dict[0]
                bias_mutations = bias_dict[0]

                # mutate weights and biases and then set them
                new_weights = MutationController.random_mutate(layer.get_weights(), weight_mutations)
                new_biases = MutationController.random_mutate(layer.get_biases(), bias_mutations)

                layer.set_weights(new_weights)
                layer.set_biases(new_biases)

    # mutate an entire population
    def mutate_models(self, models):
        for model in models:
            self.model = model
            self.mutate_model()

    def set_model(self, model):
        self.model = model
