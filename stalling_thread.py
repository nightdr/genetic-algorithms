import threading
import time


# checks if the model's xPosition has moved in the last x seconds
class StallingThread(threading.Thread):
    def __init__(self, delay, game_info_controller):
        self.running = False
        self.stalling = False
        self.delay = delay
        self.game_info_controller = game_info_controller
        self.x_pos = 0
        super().__init__()

    def start(self):
        self.running = True
        super().start()

    def run(self):
        while self.running:
            time.sleep(self.delay)
            if self.x_pos == self.game_info_controller.get_x_pos():
                self.stalling = True
            else:
                self.stalling = False
            self.x_pos = self.game_info_controller.get_x_pos()

    def stop(self):
        self.running = False

    def get_stalling(self):
        return self.stalling
